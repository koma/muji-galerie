var slideIndex = 1;
showSlide(slideIndex);

function nextSlide(n) {
	showSlide(slideIndex += n);
}

function showSlide(n) {
	var i;
	var slides = document.getElementsByClassName("one-slide");
	if (n > slides.length) {slideIndex = 1}    
	if (n < 1) {slideIndex = slides.length}
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";  
	}
	slides[slideIndex-1].style.display = "block";
}

function disableNb() {
	document.getElementsByClassName("nb")[0].style.display = "none";
	document.getElementsByClassName("color")[0].style.display = "block";
}

function enableNb() {
	document.getElementsByClassName("color")[0].style.display = "none";
	document.getElementsByClassName("nb")[0].style.display = "block";
}
